import random
import copy

def setFire(x, y, foret):
    if foret[x][y] == 0:
        foret[x][y] = 1
        return True
    else:
        return False

def continueDeBruler(foret, voisinageMoore):
    prevForet = copy.deepcopy(foret)
    width = len(foret)
    height = len(foret[0])
    for i in range(width):
        for j in range(height):
            if prevForet[i][j] < 3 and prevForet[i][j] > 0:
                foret[i][j] += 1
            elif prevForet[i][j] == 0:
                if i > 0:
                    if prevForet[i-1][j] == 1:
                        setFire(i, j, foret)
                if i < width-1:
                    if prevForet[i+1][j] == 1:
                        setFire(i, j, foret)
                if j > 0:
                    if prevForet[i][j-1] == 1:
                        setFire(i, j, foret)
                if j < height-1:
                    if prevForet[i][j+1] == 1:
                        setFire(i, j, foret)
                if voisinageMoore:
                    if i > 0 and j > 0:
                        if prevForet[i-1][j-1] == 1:
                            setFire(i, j, foret)
                    if i < width-1 and j < height-1:
                        if prevForet[i+1][j+1] == 1:
                            setFire(i, j, foret)
                    if j > 0 and i < width-1:
                        if prevForet[i+1][j-1] == 1:
                            setFire(i, j, foret)
                    if j < height-1 and i > 0:
                        if prevForet[i-1][j+1] == 1:
                            setFire(i, j, foret)

def startForet(foret, proba):
    width = len(foret)
    height = len(foret[0])
    for i in range(width):
        for j in range(height):
            if random.randrange(0,100) <= proba:
                foret[i][j] = 0
            else:
                foret[i][j] = 3

